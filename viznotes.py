"""
Script pour la visualisation des moyennes pour chaque prénom.
On calcule les moyennes de chaque prénom et on la place en x. En y, on garde toujours le nombre de candidats.
"""

import sqlite3
import matplotlib.pyplot as plt

nombreDePoints = 50

# On se connecte
db = sqlite3.connect("bac.s3db")
cur = db.cursor()

# On récupère les données dans la base de données !
# comme je fais des tests sur une base pas finie
# j'ai rajouté Prenom != '' pour ignorer les résultats pas récupérés
SQL = "SELECT * FROM Resultats WHERE Prenom != ''"
resultats = cur.execute(SQL).fetchall()

db.close()

# notre variable qui va contenir toutes les données
# je préfère tout charger puis classifier que de faire 30 000 requêtes SQL
# c'est plus rapide et au moins on a terminé avec la base de données
# on stocke dedans le nombre de chaque mention et le nombre de bacheliers qui portent le prénom
prenoms = dict()

# On analyse nos données
for academie, nom, prenom, lien, serie, resultat, mention in resultats:
    # On dégage les deuxièmes prénoms
    prenom = prenom.split()[0]

    # Si on n'a pas encore le prénom dans nos données, on l'ajoute et on initialise son compteur à 0
    if not prenom in prenoms:
        prenoms[prenom] = {"COUNT" : 0}

    # On ajoute 1 au compteur de mention correspondant à celle de la ligne
    if mention != None:
        prenoms[prenom][mention] = prenoms[prenom].get(mention, 0) + 1

    prenoms[prenom]["COUNT"] += 1

donnees = dict()
# Pour faire la moyenne, j'utilise des infos trouvées sur Internet (premier groupe)
# Note minimale      : 10/20
# Mention assez bien : [12; 14]
# Mention bien       : [14; 16]
# Mention très bien  : [16; 20+]
# Comme les notes au-dessus de 18 restent relativement rares, je pondère le TB par 17,5 plutôt que la vraie moyenne de 18
for prenom, mentions in prenoms.items():
    sansMention = mentions["COUNT"] - mentions.get("MENTION ASSEZ BIEN", 0) - mentions.get("MENTION BIEN", 0) - mentions.get("MENTION TRÈS BIEN", 0)
    donnees[prenom] = (sansMention * 11 + mentions.get("MENTION ASSEZ BIEN", 0) * 13 + mentions.get("MENTION BIEN", 0) * 15 + mentions.get("MENTION TRÈS BIEN", 0) * 17.5) / mentions["COUNT"]

# On prépare les données pour les représenter
# Ici x     = horizontal = moyenne du prénom
#     y     = vertical   = nombre de bacheliers avec ce nom
#     label = le prénom en question
x, y, label = list(), list(), list()

# - on compte chaque itération (pour sortir, on ne peut pas faire de slicing sur un générateur)
# - on reverse parce qu'on trie les prénoms par nombre de candidats et on veut ceux qui en ont le plus
# - on trie selon le nombre de candidats avec sorted
# - notre key est tout simplement le nombre de candidats ayant ce prénom
for c, prenom in enumerate(reversed(sorted(donnees.keys(), key = lambda x : prenoms[x]["COUNT"]))):
    # On se limite à nombreDePoints résultats
    if c >= nombreDePoints:
        break

    # x représente la moyenne
    x.append(donnees[prenom])

    # y représente le nombre de gens qui portent ce prénom
    y.append(prenoms[prenom]["COUNT"])

    # z représente le prénom
    label.append(prenom)

# On affiche
fig, ax = plt.subplots()
ax.scatter(x, y)

for i, txt in enumerate(label):
    ax.annotate(txt, (x[i], y[i]))

plt.show()
