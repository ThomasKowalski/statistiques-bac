import sqlite3
import requests
from bs4 import BeautifulSoup

session = requests.session()

baseUrl = "http://resultat.studyrama.com/"
home = baseUrl + "/bac"
home_content = session.get(home).text

soup = BeautifulSoup(home_content, 'lxml')
academieList = soup.find_all("ul", attrs = {'class' : 'large-2 medium-6 columns margin-top academie-list'})

academieLinks = list()
academies = dict()

for ul in academieList: 
    for li in ul.find_all("li"):
        a = li.find("a")
        academies[a.text.replace(">", "").strip()] = baseUrl + a["href"]
        academieLinks.append(baseUrl + a["href"])

# Maintenant on a tous les liens vers les différentes académies dans academieLinks.
# On va parcourir chacun
# Comme les différentes pages s'obtiennet avec [lien]/[lettre] on peut directement parcourir notre alphabet

alphabet = [chr(x) for x in range(ord('a'), ord('a') + 26)]

eleves = dict()

for academie, link in academies.items():
    eleves[academie] = list()

    print("Chargement de {}".format(link))
    for lettre in alphabet:
        print("\tChargement de la page '{}'".format(lettre))

        html = session.get("{}/{}".format(link, lettre)).text

        with open("html.html", "w") as f:
            f.write(html)

        soup = BeautifulSoup(html, 'lxml')
        ul = soup.find('ul', attrs = {'class' : 'list'})
        try:
            for li in ul.find_all("li"):
                lienEleve = li.find("a")["href"]
                nomEleve = li.find("span", attrs = {"class" : "bold name"}).text
                infoComplete = li.find("span").text
                examen = li.find("span", attrs = {"class" : "examen"}).text

                eleves[academie].append(lienEleve)
        
        except AttributeError as e:
            print("\tPas de nom en {} pour {}".format(lettre, academie))

SQL = "CREATE TABLE Eleves (Academie TEXT, Lien TEXT UNIQUE)"
db = sqlite3.connect("bac.s3db")
cur = db.cursor()

try:
    cur.execute(SQL)
except:
    pass

SQL = "INSERT INTO Eleves VALUES (?, ?)"

for academie, liste in eleves.items():
    for i, eleve in enumerate(liste):
        try:
            cur.execute(SQL, (academie, eleve))
        except:
            print("Impossible d'insérer ({}, {}) ({})".format(academie, eleve, i))

db.commit()
