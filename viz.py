"""
Script permettant la visualisation des la répartition des mentions pour les prénoms. 
On va ainsi avoir la proportion d'une mention en x, et le nombre de candidats portant ce prénom en y.
"""

import sqlite3
import matplotlib.pyplot as plt

# nombre de personnes qui doivent porter un prénom pour qu'il soit pris en compte
# mettre une valeur trop basse risque de faire apparaître des prénoms rares (Charles Philippe par exemple) 
# ce qui n'a aucun intérêt puisqu'il suffit qu'il ait mention TB pour apparaître en top
# mais ça ne représente pas une tendance (puisqu'il est tout seul)
pallier = 50
# nombre de prénoms à être représentés
# en mettre trop fait une grosse masse vers l'origine
# ça devient lisible qu'à condition de zoomer 
# mais impossible de partager l'image après
nombreDePoints = 100

# On se connecte
db = sqlite3.connect("bac.s3db")
cur = db.cursor()

# On récupère les données dans la base de données !
# comme je fais des tests sur une base pas finie
# j'ai rajouté Prenom != '' pour ignorer les résultats pas récupérés
SQL = "SELECT * FROM Resultats WHERE Prenom != ''"
resultats = cur.execute(SQL).fetchall()

db.close()

# notre variable qui va contenir toutes les données
# je préfère tout charger puis classifier que de faire 30 000 requêtes SQL
# c'est plus rapide et au moins on a terminé avec la base de données
# on stocke dedans le nombre de chaque mention et le nombre de bacheliers qui portent le prénom
prenoms = dict()

# On analyse nos données
for academie, nom, prenom, lien, serie, resultat, mention in resultats:
    # On dégage les deuxièmes prénoms
    prenom = prenom.split()[0]

    # Si on n'a pas encore le prénom dans nos données, on l'ajoute et on initialise son compteur à 0
    if not prenom in prenoms:
        prenoms[prenom] = {"COUNT" : 0}

    # On ajoute 1 au compteur de mention correspondant à celle de la ligne
    if mention != None:
        prenoms[prenom][mention] = prenoms[prenom].get(mention, 0) + 1

    prenoms[prenom]["COUNT"] += 1

# On transforme nos données

# Dans les prochaines lignes, on cherche la répartition des mentions TB
def donneesTresBien():
    tb = dict()
    for prenom in prenoms:
        if prenoms[prenom]["COUNT"] >= pallier:
            tb[prenom] = prenoms[prenom].get("MENTION TRÈS BIEN", 0) / prenoms[prenom]["COUNT"]
    return tb

# Dans les prochaines lignes, on cherche la répartition des "sans mention"
def donneesSansMention():
    aucuneMention = dict()
    for prenom in prenoms:
        if prenoms[prenom]["COUNT"] >= pallier:
            aucuneMention[prenom] = prenoms[prenom]["COUNT"] - prenoms[prenom].get("MENTION TRÈS BIEN", 0) - prenoms[prenom].get("MENTION BIEN", 0) - prenoms[prenom].get("MENTION ASSEZ BIEN", 0)
            aucuneMention[prenom] = aucuneMention[prenom] / prenoms[prenom]["COUNT"]
    return aucuneMention

# On choisit quelles données on veut représenter
donnees = donneesTresBien()

# On prépare les données pour les représenter
# Ici x     = horizontal = proportion de TB
#     y     = vertical   = nombre de bacheliers avec ce nom
#     label = le prénom en question
x, y, label = list(), list(), list()

# enumerate(reversed(sorted(tb.keys(), key = lambda x : tb[x])))
# - on compte chaque itération (pour sortir, on ne peut pas faire de slicing sur un générateur)
# - on reverse parce qu'on trie les prénoms par proportion de TB et on veut ceux qui en ont le plus
# - on trie selon le nombre de tb avec sorted
# - notre key est tout simplement la proportion de TB, soit tb[x]
for c, prenom in enumerate(reversed(sorted(donnees.keys(), key = lambda x : donnees[x]))):
    # On se limite à nombreDePoints résultats
    if c >= nombreDePoints:
        break

    # x représente la part de TB pour ce prénom
    x.append(donnees[prenom])

    # y représente le nombre de gens qui portent ce prénom
    y.append(prenoms[prenom]["COUNT"])

    # z représente le prénom
    label.append(prenom)

# On affiche
fig, ax = plt.subplots()
ax.scatter(x, y)

for i, txt in enumerate(label):
    ax.annotate(txt, (x[i], y[i]))

plt.show()
