import sqlite3
import requests
from bs4 import BeautifulSoup

session = requests.session()

baseUrl = "http://resultat.studyrama.com/"

# Pour pouvoir arrêter la récupération des données et la reprendre à posteriori (puisqu'il y a beaucoup d'élèves)
# Plutôt que de faire une opération compliquée sur la base de données à base de différence symétrique sur les élèves qui ont pas encore de résultats en faisant une jointure (...)
# On copie lors de la première exécution de ce fichier l'intégralité des élèves à récupérer dans une nouvelle table Resultats
# Ensuite, on met à jour chaque ligne
# Ainsi, pour continuer la récupération de données, il suffit de travailler sur les lignes où Série = '' par exemple.

print("Connexion à la BDD")
db = sqlite3.connect("bac.s3db")
cur = db.cursor()

# On récupère tous les liens (première fois uniquement, mais on le fait à chaque fois, au pire on a une exception rattrapée avec le try)
print("Récupération des liens")
SQL = "SELECT * FROM Eleves"
results = cur.execute(SQL).fetchall()

try:
    SQL = "CREATE TABLE Resultats (Academie TEXT, Nom TEXT, Prenom TEXT, Lien TEXT UNIQUE, Serie TEXT, Resultat TEXT, Mention TEXT)"
    print("Création de la table de données")
    cur.execute(SQL)
except:
    pass

resultats = list()

c = 0

# On copie nos données dans la nouvelle table
print("Initialisation de la table de données")
for academie, lien in results:
    try:
        cur.execute("INSERT INTO Resultats VALUES (?, ?, ?, ?, ?, ?, ?)", (academie, "", "", lien, "", "", None))
    except:
        pass

# On récupère que les élèves dont on n'a pas encore les résultats
results = cur.execute("SELECT * FROM Resultats WHERE Prenom = '' AND Nom = ''").fetchall()
print("Début de la récupération ({} restants)".format(len(results)))
for academie, nom, prenom, lien, serie, resultat, mention in results:
    c += 1

    lienBase = lien
    lien = baseUrl + lien
    resultats.append(dict())
    cetEleve = resultats[-1]
    cetEleve["Academie"] = academie
    cetEleve["Lien"] = lien

    html = session.get(lien).text 

    soup = BeautifulSoup(html, 'html.parser')
    div = soup.find("div", attrs={"class" : "callout", "id" : "laureat"})
    
    prenom = div.find_all("label", attrs = {"class" : "name black bold"})[1] # le premier label, c'est "FELICITATIONS"
    nom = prenom.find("span", attrs = {"class" : "uppercase"}).text
    prenom = prenom.contents[0].strip()

    verdict = div.find("label", attrs = {"class" : "pink verdict uppercase"})
    if verdict.find("span").text != "ADMIS":
        print(lien)
        input()
    if len(verdict.contents) > 1:
        mention = verdict.contents[1]
    else:
        mention = None
    
    serie = div.find("p", attrs = {"class" : "center black margin-none"})
    serie = " ".join(serie.text.split())
    
    # Permet de supprimer le texte qu'on a toujours : "baccalauréat général série [SERIE] ([Description acronyme]) 2017"
    serie = serie.split()[3]

    # On met à jour la ligne avec les données extraites
    cur.execute("""UPDATE Resultats 
                    SET Academie = ?,
                        Nom = ?,
                        Prenom = ?,
                        Serie = ?,
                        Resultat = ?,
                        Mention = ?
                    WHERE lien = ?""", (academie, nom, prenom, serie, "Admis", mention, lienBase))

    # Tous les 50 élèves, on commit
    if c % 50 == 0:
        print("Sauvegarde... ({} résultats trouvés)".format(c))
        db.commit()

db.commit()
print("Fini !")
input("Appuyez sur entrée")
