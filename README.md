# Statistiques BAC

## Introduction

Pour plus d'informations sur ce projet, merci de lire [mon article](https://thomaskowalski.net/2017/07/statistiques-du-bac-et-web-scraping/)

Ces trois scripts Python permettent de récupérer les résultats du bac (2017) depuis le site Studyrama.

* `viz.py` permet de visualiser les données (pour l'instant assez limité, je compte l'améliorer plus tard). 
* `obtentionliens.py` et `extractionliens.py` permettent respectivement d'obtenir une liste de liens de résultats à récupérer et de les récupérer. 

## Dépendances

Normalement, le script fonctionne avec plus ou moins n'importe quelle version (j'ai utilisé Python 3.5), puisque ses seules dépendances sont :

* MatPlotLib
* Python Requests
* BeautifulSoup
* SQLite 3

## Téléchargement des résultats

Si vous souhaitez télécharger uniquement la base de données des résultats du bac extraits, [je vous invite à cliquer ici](https://bitbucket.org/ThomasKowalski/statistiques-bac/raw/master/Bac.s3db).

*Je ne garantis en rien l'exactitude ni la complétude de ces données. Notamment, les candidats recalés ne sont pas listés, et par à cause du design de Studyrama, il y a certains candidats qui sont absents (pour peu qu'il y ait deux personnes au moins portant le même nom dans une même académie).*