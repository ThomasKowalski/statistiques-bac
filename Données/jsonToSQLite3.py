import json
import sqlite3
import os

with open("prenoms2.json", "rb") as f:
    j = json.load(f)

if os.path.isfile("Prénoms 2.s3db"):
    os.unlink("Prénoms 2.s3db")

db = sqlite3.connect("Prénoms 2.s3db")
cur = db.cursor()

SQL_CreateTable = "CREATE TABLE 'Prenoms_{}' (Prenom TEXT, Genre TEXT, Nombre INTEGER)"
SQL = "INSERT INTO 'Prenoms_{}' VALUES (?, ?, ?)"

annees = list()

for ligne in j:
    ligne = ligne["fields"]

    try:
        nombre = ligne["nombre"]
        annee = ligne["annee"]
        prenom = ligne["prenoms"]
        genre = ligne["sexe"]
    except:
        print(ligne)

    if not annee in annees:
        cur.execute(SQL_CreateTable.format(annee))
        annees.append(annee)

    cur.execute(SQL.format(annee), (prenom, genre, nombre))

tousLesPrenoms = dict()
SQL = "SELECT name FROM sqlite_master WHERE type='table'"
tables = [x[0] for x in cur.execute(SQL).fetchall()]

SQL = "SELECT * FROM '{}'"
for table in tables:
    resultats = cur.execute(SQL.format(table)).fetchall()
    for ligne in resultats:
        prenom, genre, nombre = ligne
        tousLesPrenoms[prenom] = {
            "Prénom" : prenom,
            "Genre" : genre,
            "Nombre" : tousLesPrenoms.get(prenom, dict()).get("Nombre", 0) + nombre
        }

SQL = "CREATE Table 'Prenoms_Total' (Prenom TEXT, Genre TEXT, Nombre INTEGER)"
cur.execute(SQL)
SQL = "INSERT INTO 'Prenoms_Total' VALUES (?, ?, ?)"
for dico in tousLesPrenoms.values() :
    prenom = dico["Prénom"]
    genre = dico["Genre"]
    nombre = dico["Nombre"]

    cur.execute(SQL, (prenom, genre, nombre))

db.commit()