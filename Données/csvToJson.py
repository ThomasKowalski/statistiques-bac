import csv
import json
import sqlite3
import os

resultat = list()

with open('prenoms.csv', 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    premiereLigne = True
    for prenom, genre, origine, frequence in reader:
        if premiereLigne:
            premiereLigne = False
            continue

        resultat.append({
            "Prénom" : prenom,
            "Origine" : origine,
            "Fréquence" : frequence,
            "Genre" : genre
        })

# Export en JSON
with open("prenoms.json", 'w') as output:
    json.dump(resultat, output)

# Export en SQLite 3
if os.path.isfile("Prenoms.s3db"):
    os.unlink("Prenoms.s3db")

db = sqlite3.connect("prenoms.s3db")
cur = db.cursor()

SQL = "CREATE TABLE Prenoms (Prenom TEXT, Genre TEXT, Origine TEXT, Frequence REAL)"
cur.execute(SQL)

SQL = "INSERT INTO Prenoms VALUES (?, ?, ?, ?)"
for item in resultat:
    prenom = item["Prénom"]
    origine = item["Origine"]
    frequence = item["Fréquence"]
    genre = item["Genre"]
    
    cur.execute(SQL, (prenom, genre, origine, frequence))

db.commit()